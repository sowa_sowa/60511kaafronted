import { createRouter, createWebHistory } from 'vue-router'
import Homepage from './components/Homepage'
import Room from "./components/Room";
import LoginDialog from "./components/LoginDialog";

const routes = [
    {
        path: '/',
        component: Homepage
    },

    {
        path: '/room',
        component: Room,
    },
    {
        path: '/login',
        component: LoginDialog,
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router;