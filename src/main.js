import { createApp } from 'vue'
import axios from 'axios';
import router from './router.js'
import App from './App.vue'
import store from './state.js';
import PrimeVue from 'primevue/config';
import ConfirmationService from 'primevue/confirmationservice';
import Tooltip from 'primevue/tooltip';

import 'primevue/resources/themes/bootstrap4-light-blue/theme.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'primevue/resources/primevue.min.css'
;

const myApp = createApp(App);

window.axios = axios;

myApp.use(PrimeVue)
myApp.use(store)
myApp.use(router)
myApp.use(ConfirmationService);
myApp.directive('tooltip', Tooltip);
myApp.mount('#app');